package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"rtb2/lib/logger"
	"rtb2/types"
	"runtime"
	"strings"
	"time"
)

func main() {

	tick := time.Tick(2 * time.Second)
	go func() {
		for _ = range tick {
			//fmt.Println(runtime.NumGoroutine())
		}
	}()

	handlers := http.NewServeMux()
	handlers.Handle("/get", http.HandlerFunc(getHandler))
	handlers.Handle("/clickunder", http.HandlerFunc(clickunderHandler))
	handlers.Handle("/banner", http.HandlerFunc(bannerHandler))
	handlers.Handle("/video", http.HandlerFunc(videoHandler))
	handlers.Handle("/notification", http.HandlerFunc(notificationHandler))
	handlers.Handle("/statusOk", http.HandlerFunc(statusOkHandler))

	srv := &http.Server{
		Addr:         "127.0.0.1:3002",
		Handler:      handlers,
		WriteTimeout: time.Second * 50,
		ReadTimeout:  time.Second * 50,
	}
	fmt.Printf("Server pid=%d started with processes: %d \n",
		os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()))

	log.Fatal(srv.ListenAndServe())
}

func tfuncHeandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte{})
}

func notificationHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte{})
}

func getHandler(w http.ResponseWriter, r *http.Request) {

	ctx, _ := context.WithTimeout(r.Context(), time.Second*100)
	go func() {
		for _ = range time.Tick(time.Second) {
			select {
			case <-ctx.Done():
				fmt.Println(ctx.Err())
				os.Exit(1)
			default:
				fmt.Println("123")
			}
		}
	}()
	str := fmt.Sprintf(`{"bid": %+v, "url": "http://google.com" }`, 1.6799999237060548)
	rawIn := json.RawMessage([]byte(str))
	bytes, err := rawIn.MarshalJSON()
	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-type", "application/json")
	w.Write([]byte(bytes))
}

func statusOkHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	w.Write([]byte("ok"))
}

func clickunderHandler(w http.ResponseWriter, r *http.Request) {

	tn := time.Now()
	prices := []float64{100}
	price := prices[rand.Intn(len(prices))]

	var (
		err         error
		bytes       []byte
		contentType = r.Header.Get("Content-Type")
	)

	//w.Header().Set("Content-Type", r.Header.Get("Content-Type"))

	if strings.Contains(contentType, "application/json") {
		str := fmt.Sprintf(`{
				"cur":"RUB",
				"seatbid":[
					{"bid":[{
							"id":"9b759040321a408a5c7768b4511287a6",
							"price":%+v,
							"nurl": "",
							"adm":"http://localhost:3002/statusOk?
									auction_price=${AUCTION_PRICE}&
									auction_id=${AUCTION_ID}&
									auction_bid_id=${AUCTION_BID_ID}&
									auction_ad_id=${AUCTION_AD_ID}&
									auction_cur=${AUCTION_CURRENCY}&
									auction_loss=${AUCTION_LOSS}"
							}]
					}]
				}`, price)
		for _, key := range []string{"\t", "\n", " "} {
			str = strings.Replace(str, key, "", -1)
		}
		rawIn := json.RawMessage([]byte(str))
		bytes, err = rawIn.MarshalJSON()
		if err != nil {
			panic(err)
		}
		w.Write(bytes)
	}

	if r.URL.Query().Get("xml") != "" {
		var str string
		if r.URL.Query().Get("xml") == "1" {
			str = fmt.Sprintf(`<xml version="9.8.1">
										<result requesttime="165.071591ms">
											<record>
												<title>&#xA;</title>
												<desc>&#xA;</desc>
												<url>&#xA;</url>
												<clickurl>http://localhost:3002/statusOk</clickurl>
												<bid>%+v</bid>
												<pixel>://xml.ppc.buzz/getImage?data=bGFDaw==</pixel>
											</record>
										</result>
									</xml>`, price)
		}
		if r.URL.Query().Get("xml") == "2" {
			str = fmt.Sprintf(`<?xml version="1.0" encoding="UTF-8"?>
										<results>
										   <result>
										      <title><![CDATA[News and Promotions]]></title>
										      <description><![CDATA[NASA Designs Ape-Like Robot for DARPA Competition]]></description>
										      <url><![CDATA[www.newsandpromotions.com/]]></url>
										      <curl><![CDATA[http://eu1.trfilter.info/go.php?hash=4Ol63tKEYCpJMigNFsy46T5mf5OvR5e3OQEt26WIqhQrAJEhFz2iYAQ%2BIqFv7BBG%2FXrEFxVODmCjyClp%2B1nvSZ6U90J3kblpJpUFWWPCTGLpkpKVDCqsmQecw2ioV8BIo271HoMVPQQ3RJezaJXpY3zKzaut5OK8oO7zCJeAltZb6UfJrOoGvP7QJjOdp%2F4e%2FcdvLEE1OhqTnnSJItGCA3QcGfhNBznkAnBGIEw5l2%2By4YZyRW7Key%2BOj5PAlT20XIf9gwmBn0bf9n3%2F%2FB2Py9jvjD5htU8JGDXSzRKN1VBQlGZbSjLJ%2FYARW0mOYdClqXk1k623qjjc0SYhP%2BPVwjAgPxzqOH4wtGp%2BCVnOGnIY7LcZo4XQ1IwRpUz5B6OWenl1BDfyb29owwXHj8cK2R%2BRk56d%2BgYEynlwx75EQBEEgXfuh5XdKUmAUqDLiModVvTBaGSgdOowhGESTHvtBH3FUZLtAJbOcVMnLIPXkVPeGGz7pgTBrf8gXCzGboWfapn4Z7%2FRhaa%2BO7Bx3pRgVC8oNdiCx0rT3EwfGJ3Vykc8bMkxWrfGoYMF5Qh%2Bt4p79wf9huSuZRIR6fz0uYPDil9KM4EVmPGjGbu5hGkgipUREfx2fQVwAQB419BoMFgJWTRa3BBtqwqGf7YGLcZaoCPrqTPEhodU5SBipaCa0UaNO%2BAQosvsS9u7qLZ57zVm]]></curl>
										      <cpc><![CDATA[0.0002942]]></cpc>
										   </result>
										</results>`)
		}
		for _, key := range []string{"\t", "\n"} {
			str = strings.Replace(str, key, "", -1)
		}
		w.Write([]byte(str))
	}
	if time.Since(tn).Seconds() > 1 {
		fmt.Println(time.Since(tn))
	}
}

func bannerHandler(w http.ResponseWriter, r *http.Request) {

	per := []time.Duration{140, 200}
	time.Sleep(time.Millisecond * per[rand.Intn(len(per))])

	prices := []float64{1}

	price := prices[rand.Intn(len(prices))]
	str := fmt.Sprintf(`{"cur":"USD","seatbid":[{"bid":[{"id":"9b759040321a408a5c7768b4511287a6","price":%+v,"adm":"<script>console.log('скрипт отработал!!!')</script><a target='_blank' href='http://localhost:3002'><img src='http://rt-ttt.ru/image/banner/8284/284/59e84c8432f73t1508396164r6806.jpg' width='300' height='250'></a>"}]}]}`, price)
	rawIn := json.RawMessage([]byte(str))
	bytes, err := rawIn.MarshalJSON()
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	var b types.BidRequest
	if err := json.NewDecoder(r.Body).Decode(&b); err != nil {
		fmt.Println(err)
	}
	logger.Log.Infof("Name: %+v price %+v", b.Pub.Name, price)

	w.Header().Set("Content-type", "application/json")
	w.Write([]byte(bytes))
}

func videoHandler(w http.ResponseWriter, r *http.Request) {
	var str string
	if r.URL.Query().Get("xml") == "1" {
		str = fmt.Sprintf(`<?xml version="1.0" encoding="UTF-8"?>
<VAST xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0">
		<Ad id="1560856117">
		    <InLine>
		        <AdSystem>RS</AdSystem>
		        <AdTitle>RS Company</AdTitle>
		        <Error><![CDATA[]]		></Error>
		        <Impression/>
		        <Creatives>
		            <Creative>
		                <Linear>
		                    <Duration>00:00:30</Duration>
		                    <TrackingEvents></TrackingEvents>
		                    <AdParameters><![CDATA[{"jsProxy":"http:\/\/moevideo.biz\/embed\/player\/1529\/vpaid\/vpaid-bridge.min.js","frame":"http:\/\/moevideo.biz\/embed\/vpaid?token=kIjq9vhMzAiYbGoSvdrCJpRPDTF3Iq3KsRe2XFUnS436Hle9KUw8Xpeg2L5Q%2FcJS&ref=digitalfrontier.net&title=&duration=0&test=0&proxyVars%5Breferer%5D=%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5_%D1%81%D0%B0%D0%B9%D1%82%D0%B0&impressionAfterPaid=1&es=1&mvver=1529"}]]></AdParameters>
		                    <MediaFiles>
		                    	<MediaFile apiFramework="VPAID" type="application/x-shockwave-flash"><![CDATA[http://moevideo.biz/embed/player/1529/swf/vpaid-player-new.swf?adParams=%7B%22jsProxy%22%3A%22http%3A%5C%2F%5C%2Fmoevideo.biz%5C%2Fembed%5C%2Fplayer%5C%2F1529%5C%2Fvpaid%5C%2Fvpaid-bridge.min.js%22%2C%22frame%22%3A%22http%3A%5C%2F%5C%2Fmoevideo.biz%5C%2Fembed%5C%2Fvpaid%3Ftoken%3DkIjq9vhMzAiYbGoSvdrCJpRPDTF3Iq3KsRe2XFUnS436Hle9KUw8Xpeg2L5Q%252FcJS%26ref%3Ddigitalfrontier.net%26title%3D%26duration%3D0%26test%3D0%26proxyVars%255Breferer%255D%3D%25D0%25BD%25D0%25B0%25D0%25B7%25D0%25B2%25D0%25B0%25D0%25BD%25D0%25B8%25D0%25B5_%25D1%2581%25D0%25B0%25D0%25B9%25D1%2582%25D0%25B0%26impressionAfterPaid%3D1%26es%3D1%26mvver%3D1529%22%7D]]></MediaFile>
								<MediaFile apiFramework="VPAID" type="application/javascript"><![CDATA[http://moevideo.biz/embed/player/1529/vpaid/vpaid.min.js]]></MediaFile>
		                    </MediaFiles>
		                </Linear>
		            </Creative>
		        </Creatives>
		        <Extensions>
		            <Extension type="controls"><![CDATA[0]]></Extension>
		            <Extension type="isClickable"><![CDATA[0]]></Extension>
		        </Extensions>
		    </InLine>
		</Ad></VAST>`)
	}
	if r.URL.Query().Get("xml") == "2" {
		str = fmt.Sprintf(`<?xml version="1.0" encoding="UTF-8"?>
<VAST version="3.0">
	<Ad>
        <InLine>
            <AdSystem>UnionTraff</AdSystem>
            <AdTitle>UTraff Ad</AdTitle>
            <Impression/>
            <Creatives>
                <Creative >
                    <Linear>
                        <AdParameters>
                            <![CDATA[{ "vid": "Pu1qhXO6uzGe2DsKH5IKz-3J6NuJ30lAfkAwQjXgF9Y" }]]>
                        </AdParameters>
                        <TrackingEvents/>
                        <Duration>00:00:30</Duration>
                        <MediaFiles>
                            <MediaFile height="360" width="640" bitrate="" type="application/javascript" delivery="progressive" maintainAspectRatio="true" scalable="true" apiFramework="VPAID">
                                <![CDATA[//utraff.com/vpaidp/bundle.7fb6a714.js]]>
                            </MediaFile>
                        </MediaFiles>
                    </Linear>
                </Creative>
            </Creatives>
        </InLine>
    </Ad>
</VAST>`)
	}
	for _, key := range []string{"\t", "\n"} {
		str = strings.Replace(str, key, "", -1)
	}
	w.Write([]byte(str))
}
