# coding: utf-8
import datetime

from fabric.api import env, local, put, run, sudo

""" установка переменных тестового сервера """
env.hosts = ['localhost']
env.user = 'ubuntu'
env.project_root = '/opt/rtb'


def deploy():
    """ деплой приложения """
    print(datetime.datetime.now())

    local("env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ./bin/dsp -a -installsuffix cgo -ldflags '-s'")

    put('bin/dsp', '/tmp/dsp', mode=0755)
    sudo("service dsp stop")
    run('mv /tmp/dsp /opt/rtb')
    sudo("service dsp start")
